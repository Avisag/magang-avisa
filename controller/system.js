// Province System
var dataProvinsi = angular.module('province', []);  
dataProvinsi.controller('dataProvince', function($scope) {  
  $scope.provinsi = [{provinsi:'Aceh'},{provinsi:'Jawa Barat'},{provinsi:'Bali'},
                     {provinsi:'Riau'},{provinsi:'Jawa Tengah'},{provinsi:'Jambi'},
                     {provinsi:'DKI Jakarta'},{provinsi:'Jawa Timur'},{provinsi:'Banten'}
                    ]
});
// Close Province System
// City System
var dataKota = angular.module('city', []);
dataKota.controller('dataCity', function($scope) {
  $scope.kota = [{kota:'Banda Aceh'},{kota:'Bandung'},{kota:'Denpasar'},
                 {kota:'Riau'},{kota:'Semarang'},{kota:'Jambi'},
                 {kota:'Jakarta'},{kota:'Surabaya'},{kota:'Banten'}
                ]
});
// Close City System
